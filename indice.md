# Documentación de procesos

Los documentos con id en `negrita` los añade el profesor al classroom.

## Semana 19/06/23

+ 190623-001 - Proceso paso a paso para obtener información básica de hardware y software en Windows 10.
+ 190623-002 - Proceso paso a paso para crear un medio de instalación de Windows 10 (ISO y unidad extraíble).
+ 190623-003 - Método genérico para arrancar desde un dispositivo extraíble (sin entrar en la BIOS).
+ **`190623-004`** - Método genérico para entrar en la BIOS.
+ 190623-005 - Proceso paso a paso genérico para cambiar el orden de arranque en la BIOS.
+ 190623-006 - Proceso paso a paso para instalar Windows 10.
+ 190623-007 - Método para abrir una ventana de comandos desde el instalador de Windows 10.
+ 190623-008 - Proceso paso a paso para parar un servicio de Windows y cambiar su tipo de inicio con la utilidad de Windows servicios.
+ **`190623-009`** - Método para parar un servicio de Windows y cambiar su tipo de inicio desde un terminal.
+ 190623-0010 - Método para limpiar las particiones de una unidad de almacenamiento (extraíble o no) con la utilidad de Windows diskpart.

## Semana 16/10/23

+ 161023-011 - Proceso paso a paso para crear un medio de instalación de MiniOs (MBR y GPT).
+ 161023-012 - Proceso paso a paso de instalación de MiniOs.
+ 161023-013 - Proceso paso a paso para crear un medio de instalación de Chrome Os Flex.
+ 161023-014 - Proceso paso a paso de instalación de Chrome Os Flex (hasta ese paso donde paramos).
+ 161023-015 - Método para arrancar desde un dispositivo extraíble en tablets HP x2 210 G2.
+ 161023-016 - Método para entrar en la BIOS en tablets  HP x2 210 G2.
+ 161023-017 - Método para activar Windows.
+ 161023-018 - Método para activar Office 2016.

## Semana 22/01/24

+ 220124-019 - Proceso paso a paso para iniciar la restauración de sistema de Windows 10 dejándolo en estado de fábrica (desde windows).
+ 220124-020 - Proceso paso a paso para iniciar la restauración de sistema de Windows 10 dejándolo en estado de fábrica (desde modo a prueba de fallos).
+ **`220124-021`** - Proceso paso a paso de clonación de discos duros por hardware.
+ 220124-022 - Proceso paso a paso para deshabilitar la cámara en Windows 10.

## Semana 05/02/24

+ **`050224-023`** - Método para borrar la clave de la BIOS de portátiles Toshiba NB200.
+ 050224-024 - Proceso paso a paso para crear un medio de instalación de MiniOs MBR (Linux)
+ 050224-025 - Proceso paso a paso para instalar Minios (Linux) en un ordenador.
+ 050224-026 - Proceso paso a paso para configurar el teclado de MiniOs, instalar openoffice y mupdf
